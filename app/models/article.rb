class Article < ApplicationRecord
    include Visible

    has_many :comments

    validates :title, presence: true
    validates :body, presence: true, length: { minimum: 5 }
   
end
=begin
has_many :comments et le belongs_to :article
Ces deux déclarations permettent un bon comportement automatique. Par exemple, si vous avez une variable d'instance @article contenant un article, vous pouvez récupérer tous les commentaires appartenant à cet article sous forme de tableau à l'aide de @article.comments.   
=end
